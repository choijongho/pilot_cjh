import sys
import os
#sys.path.append(os.path.dirname(os.path.abspath(os.path.dirname(__file__))))
sys.path.append(os.path.dirname(__file__) + '/../pilot_stackCalculator')
print (os.path.dirname(__file__) + '/../pilot_stackCalculator')

from calculator import get_tokens, calculate_ops

"""
usage : python ll1parser.py "3*(7 + 4*8) / (5 * 2-1) + (-5)*(5*2)"
"""

"""
expr := <term> (<add> <expr>)?
term := <factor> (<multi> <term>)?
factor := '(' <expr>' )'
factor := NUMBER
factor := '-', <factor>
add := '+', '-'
multi := '*', '/'
"""

token = list()

def debugger(*s):
    #print(s)
    pass


class Nonterminal:
    def __init__(self):
        self.childs = []          # token을 분해하여 객체화해서 append 해두는 리스트
        self.value = 0            # calculate()에서 계산하여 저장할 값
        self.tokens = []

    def get_next_token(self):
        global token
        ret = token[0]
        token = token[1:]
        self.tokens.append(ret)
        return ret

    def show_next_token(self):
        global token
        ret = token[0]
        return ret

    def is_next_token(self):
        global token
        if len(token) > 0:
            return True
        else:
            return False

    def is_string_add(self, tok):
        if tok == '+' or tok == '-':
            return True
        else :
            return False

    def is_string_multi(self, tok):
        if tok == '*' or tok == '/':
            return True
        else:
            return False

    def add_childs(self, newobjects):
        self.childs.append(newobjects)

    def parse(self):
        if len(self.tokens) == 0:
            self.type = 'empty'
            return

    def calculate(self):
        return 0

class Expr(Nonterminal):
    def __init__(self):
        super(self.__class__, self).__init__()

    def parse(self):
        """
        do : expr의 구성 요소 를 각 객체로 만들어 self.child에 add
        """
        term = ExprTerm()
        term.parse()
        self.add_childs(term)
        if self.is_next_token():
            if self.is_string_add(self.show_next_token()):
                token = self.get_next_token()
                add = Term('add', token)
                self.add_childs(add)
                expr = Expr()
                expr.parse()
                self.add_childs(expr)
        debugger("Expr parse term : ", term.tokens)

    def calculate(self):
        """
        calculated self.value using self.childs 's value
        """
        term = self.childs[0]
        term.calculate()
        if len(self.childs) > 1:
            ops = self.childs[1]
            expr = self.childs[2]
            expr.calculate()
            debugger("?", term.value, ops.value, expr.value)
            self.value = calculate_ops(term.value, ops.value, expr.value)
        else:
            self.value = term.value

class ExprTerm(Nonterminal):
    def __init__(self):
        super(self.__class__, self).__init__()

    def parse(self):
        factor = Factor()
        factor.parse()

        self.add_childs(factor)

        if self.is_next_token():
            next_token = self.show_next_token()
            if self.is_string_multi(next_token):
                token = self.get_next_token()
                multi = Term('multi', token)
                self.add_childs(multi)
                term = ExprTerm()
                term.parse()
                self.add_childs(term)
        debugger("ExprTerm parse factor ", factor.tokens)

    def calculate(self):
        """
        self.value 의 값을 self.childs 의 각 value로 계산
        """
        factor = self.childs[0]
        factor.calculate()
        if len(self.childs) > 1:
            multi = self.childs[1]
            term = self.childs[2]
            term.calculate()
            debugger("!", factor.value, multi.value, term.value)
            self.value = calculate_ops(factor.value, multi.value, term.value)
        else:
            self.value = factor.value


class Factor(Nonterminal):
    def __init__(self):
        super(self.__class__, self).__init__()

    def parse(self):
        token = self.get_next_token()

        if type(token) == float:
            self.add_childs(Term("number", token))

        elif token == '(':
            # ( <expr> )
            self.add_childs(Term("parentheses", token))
            expr = Expr()
            expr.parse()
            self.add_childs(expr)
            token = self.get_next_token()
            self.add_childs(Term("parentheses", token))

        elif token == '-':
            term = Term('sign', '-')
            self.add_childs(term)
            factor = Factor()
            factor.parse()
            self.add_childs(factor)

        debugger("factor parse ", self.tokens)

    def calculate(self):

        len_child = len(self.childs)
        if len_child == 1:
            self.value = self.childs[0].value
        elif len_child == 2:
            factor = self.childs[1]
            factor.calculate()
            self.value = -factor.value
        elif len_child == 3:
            expr = self.childs[1]
            expr.calculate()
            self.value = expr.value


class Term:
    """
    Term : 연산자, 숫자값 등의 terminal
    'number'
    'add'
    'multi'
    'sign'
    'parentheses'
    """
    def __init__(self, nType, value):
        self.type = nType
        self.value = value
        debugger("Term Created", self.type, self.value)


TreeDepth = 0
def printTree(nonterm):
    global TreeDepth
    localdepth = TreeDepth
    TreeDepth += 1
    if len(nonterm.childs) > 0:
        for child in nonterm.childs:
            if type(child) != Term:
                debugger(localdepth, type(child), len(child.childs), child.tokens)
                printTree(child)
            else:
                debugger(localdepth, child.type, child.value)

def main():
    global token
    #strExpression = "3 + 4 / (5 * 2-1) -5"
    strExpression = "3*(7 + 4*8) / (5 * 2-1) + (-5)*(5*2)"
    #strExpression = "-(-(2+(-2+7*5)+2)*5)"
    if len(sys.argv) > 1:
        strExpression = sys.argv[1]
    print(strExpression)

    expr = Expr()
    token = get_tokens(strExpression)
    print(token)
    expr.parse()
    expr.calculate()
    printTree(expr)
    print(expr.value)

if __name__ == "__main__":
    main()
