import sys
"""
Usage : python calculator.py "3+2-7"
argument가 없으면 미리 지정된 식 사용
"""
"""
현재 있는 문제점 : 음수 피연산자의 입력이 어려움
해결 방안 : get_tokens 수정
"""


def get_tokens(strExpression):
    """
    strExpression : 괄호, 연산자, 숫자 등을 포함한 입력 식
    return : 숫자, 연산자가 분리된 리스트 (infix)
    """
    NUMBERS = "0123456789."
    tokens = []
    token_buff = ""
    strExpression = strExpression.replace(" ", "")

    for i, ch in enumerate(strExpression):

        if ch not in NUMBERS:
            if token_buff != "" :
                tokens.append(float(token_buff))
                token_buff = ""
            tokens.append(ch)
        else:
            token_buff += ch

    if token_buff != "":
        tokens.append(float(token_buff))
        token_buff = ""
    return tokens


def ops_priority(ops):
    if ops == '+' or ops == '-':
        return 0
    elif ops == '*' or ops == '/':
        return 1
    elif ops == ")":
        return 2
    elif ops == "(":
        return -1
    else:
        print("Invalid ops", ops)
        return -2


def sort_tokens_into_postfix(infix_tokens):
    """
    infix_tokens : get_tokens 에서 변환된 토큰 리스트
    return : infix 가 postfix로 바뀐 리스트
    """
    stack = []
    postfix_tokens = []
    for token in infix_tokens:
        # print("token ", token)
        # print("stack ", stack)
        # print("pofix ", postfix_tokens)
        if type(token) is float:
            postfix_tokens.append(token)
        elif token == "(":
            stack.append(token)
        elif token == ")":
            while stack[-1] != "(":
                postfix_tokens.append(stack.pop())
            stack.pop()
        elif token == '+' or token == '-' or token == '/' or token == '*':
            while len(stack) > 0 and ops_priority(token) <= ops_priority(stack[-1]):
                postfix_tokens.append(stack.pop())
            stack.append(token)

    while len(stack) > 0:
        postfix_tokens.append(stack.pop())

    return postfix_tokens


def calculate_ops(a, ops, b):
    """
    a, b : 피연산자
    ops : 연산자
    """
    if ops == "+":
        return a + b
    elif ops == "-":
        return a - b
    elif ops == "*":
        return a * b
    elif ops == "/":
        return a / b


def calculate_postfix(postfix_tokens):
    """
    postfix_tokens : postfix
    return : result value(float)
    """
    stack = []
    for token in postfix_tokens:
        if type(token) is float:
            stack.append(token)
        else:
            if len(stack) > 1:
                num2 = stack.pop()
                num1 = stack.pop()
                rt = calculate_ops(num1, token, num2)
                stack.append(rt)
            else:
                print("Invalid calculation", stack, token)


    return stack[-1]



def main():

    # expression = "3+7/2*4-2/5+((2+3/4)/(2+1))"
    if len(sys.argv) == 1:
        expression = "( -4 / 5)--2 + -2 - (453) * (4.42 + 0.03)"
    else:
        expression = sys.argv[1]
    infix_tokens = get_tokens(expression)
    postfix_tokens = sort_tokens_into_postfix(infix_tokens)
    print("infix : ", infix_tokens)
    print("postfix : ", postfix_tokens)
    print("result : ", calculate_postfix(postfix_tokens))
    return 0


if __name__ == "__main__":
    main()
